<?php
require_once '../vendor/autoload.php';

session_start();

$client = new Google\Client();
$client->setAccessType('offline');
$client->setAuthConfig('credentials.json');
$client->setScopes(['https://www.googleapis.com/auth/gmail.readonly','https://mail.google.com/','https://www.googleapis.com/auth/gmail.modify','https://www.googleapis.com/auth/gmail.compose','https://www.googleapis.com/auth/gmail.send']);

$client->setRedirectUri('http://127.0.0.1/gmail/examples/oauth2callback.php');
 
if (! isset($_GET['code'])) {
  $auth_url = $client->createAuthUrl();
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} else {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/gmail/examples/login.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}