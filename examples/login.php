<?php
session_start();

require_once '../vendor/autoload.php';
require_once 'includes/config.php';

//Array ( [access_token] => ya29.a0ARrdaM9tlvprez0SFLfpHwd83eSyQbPE6HML3hAEF7FXKnnMyT7PhukmOgqAzMMdQA8GL7ljwH_y3IjIbXqEjh0qwW_fjQLc4NPZWnAyuS2loiTKO8IFoqMZETIqznEQ1KKeGMfBjlG1x-WDVCQo8s_J5om- [expires_in] => 3599 [refresh_token] => 1//05T0ttSW3lIWcCgYIARAAGAUSNwF-L9IrVHXaWvCxCkc-BkNpoKFECpN8e3HV7PZzMd9jm7rqk2fnOgwFFSN8bxYZ9htJPud-r7g [scope] => https://www.googleapis.com/auth/gmail.addons.current.message.readonly [token_type] => Bearer [created] => 1654878373 )

$client = new Google\Client();

$client->setAccessType('offline');
$client->setAuthConfig('credentials.json');
$client->setScopes(['https://www.googleapis.com/auth/gmail.readonly', 'https://mail.google.com/', 'https://www.googleapis.com/auth/gmail.modify', 'https://www.googleapis.com/auth/gmail.compose', 'https://www.googleapis.com/auth/gmail.send']);
$client->setRedirectUri('http://127.0.0.1/gmail/examples/oauth2callback.php');

$tok = '{"access_token":"ya29.a0ARrdaM8zPu08il5bi-mq5gaR1ow5_dCk7K9z1EYycXdB9YywMNqr5ic-zXzBbNxuCtJqTySg1LeS_E9VvTmF2gJWQDiFfREn8q5RG-28ZF1fDMvdLy8YWyYViH7G902hTIKtPw8fUznQkYBSPtLzWJpWTGMF","expires_in":3599,"refresh_token":"1\/\/05mM-7gRR6ivwCgYIARAAGAUSNwF-L9IrnAz-R_H5V0epZDUlO1kbUxeDwVs2SZpFlIkRDbyxE6qbF6sjSrlIb3KE3Ud-JKpW8pY","scope":"https:\/\/www.googleapis.com\/auth\/gmail.compose https:\/\/www.googleapis.com\/auth\/gmail.send https:\/\/www.googleapis.com\/auth\/gmail.readonly https:\/\/www.googleapis.com\/auth\/gmail.modify https:\/\/mail.google.com\/","token_type":"Bearer","created":1655234548}';
//echo  $tok;
$tokenPath = 'token.json';
if ($tok) {
    $accessToken = json_decode($tok, true);
    $client->setAccessToken($accessToken);
    $_SESSION['access_token'] = $client->getAccessToken();

    print_r($_SESSION['access_token']);
} else {
    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/gmail/examples/oauth2callback.php';
    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
