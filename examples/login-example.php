<?php
session_start();

require_once '../vendor/autoload.php';
require_once('includes/config.php');

 //Array ( [access_token] => ya29.a0ARrdaM9tlvprez0SFLfpHwd83eSyQbPE6HML3hAEF7FXKnnMyT7PhukmOgqAzMMdQA8GL7ljwH_y3IjIbXqEjh0qwW_fjQLc4NPZWnAyuS2loiTKO8IFoqMZETIqznEQ1KKeGMfBjlG1x-WDVCQo8s_J5om- [expires_in] => 3599 [refresh_token] => 1//05T0ttSW3lIWcCgYIARAAGAUSNwF-L9IrVHXaWvCxCkc-BkNpoKFECpN8e3HV7PZzMd9jm7rqk2fnOgwFFSN8bxYZ9htJPud-r7g [scope] => https://www.googleapis.com/auth/gmail.addons.current.message.readonly [token_type] => Bearer [created] => 1654878373 )

$client = new Google\Client();

$client->setAccessType('offline');
$client->setAuthConfig('credentials.json');
$client->setScopes(['https://www.googleapis.com/auth/gmail.readonly','https://mail.google.com/','https://www.googleapis.com/auth/gmail.modify','https://www.googleapis.com/auth/gmail.compose','https://www.googleapis.com/auth/gmail.send']);
  $client->setRedirectUri('http://127.0.0.1/gmail/examples/oauth2callback.php');
 

$tokenPath = 'token.json';
if (file_exists($tokenPath)) {
    $accessToken = json_decode(file_get_contents($tokenPath), true);
    $client->setAccessToken($accessToken);
}


if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  $client->setAccessToken($_SESSION['access_token']);
  print_r($_SESSION['access_token']);

  $tokenPath = 'token.json';
  if (!file_exists(dirname($tokenPath))) {
    mkdir(dirname($tokenPath), 0700, true);
}
file_put_contents($tokenPath, json_encode($client->getAccessToken()));

 // $drive = new Google\Service\Drive($client);
  //$files = $drive->files->listFiles(array())->getItems();
  //echo json_encode($files);
} else {
   $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/gmail/examples/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
